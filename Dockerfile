FROM alpine:3.7
ENV WORKDIR=/khcheck
RUN mkdir $WORKDIR

RUN apk add --no-cache bash curl jq

RUN adduser khcheck -h /khcheck -D
USER khcheck
WORKDIR $WORKDIR

COPY ./docker-entrypoint.sh /

ENTRYPOINT ["/docker-entrypoint.sh"]

