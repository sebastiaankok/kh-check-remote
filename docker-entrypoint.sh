#!/bin/bash

echo "$KH_REPORTING_URL"
echo "$KH_CHECK_RUN_DEADLINE"
echo "$KH_ENDPOINT"

function postResult () {
        local error_msg="$1"

        if [ -n "$error_msg" ]; then
        	echo "Error(s) found: $error_msg"
                curl -s -X POST -H 'Content-Type: application/json' \
		--data '{"Errors":["'"$error_msg"'"],"OK":false}' "$KH_REPORTING_URL"
                exit 1
        else
		echo "No errors found."
                curl -s -X POST -H 'Content-Type: application/json' \
		--data '{"Errors":[],"OK":true}' "$KH_REPORTING_URL"
                exit 0
        fi
}

json_status="$(curl -s $KH_ENDPOINT -H "Host:kh.monitoring.true.nl")"

if [ -z "$json_status" ]; then
	postResult "Error retrieving JSON data from endpoint address: $KH_ENDPOINT"
fi

error_array=()

## -- Loop over each check element.
for check in $(jq '.CheckDetails' <<< "$json_status" | jq -r 'keys[] as $k | "\($k)"'); do

	## -- Check OK status
 	check_output="$(jq '.CheckDetails' <<< "$json_status" | jq '."'$check'"')"
	check_status="$(jq '.OK' <<< "$check_output")"

	## -- Add errors found to error array
	if [ "$check_status" != "true" ] ; then
		## -- Remove unwanted spaces and characters that break the JSON payload.
		check_errors="$(jq '.Errors' <<< "$check_output" | tr -d ']["\n' | tr -d "'" | sed -e 's/^[ \s]*//g')"
		error_array+="check: $check - $check_errors"
	fi
done

## -- POST JSON result to KuberHealthy endpoint.
postResult "${error_array[*]}"
